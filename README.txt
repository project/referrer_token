Referrer Token
  by James Forbes Keir, james@website-express.co.uk
-----------------------------------------

Description:
-----------------------------------------
This module makes HTTP Referrer tokens available for the current page

Requirements:
-----------------------------------------
As this module is an add-on to Token, it requires the 
Token module. https://www.drupal.org/project/token

Installation:
-----------------------------------------
Install in the usual manner:
- Unpack the module folder into your modules directory
- Go to /admin/modules and enable.

Configuration:
-----------------------------------------
None - enabling it is enough.

Usage
-----------------------------------------
After enabling the module, the following two tokens are made available:
- [server:HTTP_REFERER]
- [server:HTTP_USER_AGENT]

For use in a hidden Webform module, select the hidden type to be "Hidden Element"
rather than "Secure value" to see the correct result submitted.

Credits
-----------------------------------------
Based on code from helmo in the following Token module issue:
https://www.drupal.org/project/token/issues/2021969
